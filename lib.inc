%define STATUS_EXIT 60
%define STD_IN 0
%define STD_OUT 1
%define STR_END 10
%define TAB 9
%define SPACE 32
%define ASCII 48
%define SIGN 45
%define MAX_NUM 58
%define MIN_NUM 47

section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov     rax, STATUS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor   rax, rax
.step:
    cmp  byte [rdi + rax], STD_IN
    je   .end
    inc  rax
    jmp .step
.end:
    ret

; Принимает код символа и выводит его в STDOUT
print_char:
    push    rdi
    mov     rsi, rsp
    mov     rax, STD_OUT
    mov     rdi, STD_OUT
    mov     rdx, STD_OUT
    syscall
    pop     rdi
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в STDOUT
print_string:
    mov     r9, rdi
    mov     rax, STD_OUT
    mov     rdi, STD_OUT
.val:
    cmp     byte [r9], STD_IN
    je      .end
    mov     rsi, r9
    mov     rdx, STD_OUT
    syscall
    inc     r9
    jmp     .val
.end:
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rsp, STR_END

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    xor rcx, rcx
    dec rsp
    mov [rsp], al
    mov r10, STR_END
    mov rax, rdi
.create_number:
    xor rdx, rdx
    div r10
    add dl, ASCII
    dec rsp
    mov [rsp], dl
    inc rcx
    test rax, rax
    jnz .create_number
.print:
    mov rdi, rsp
    push rcx
    call print_string
    pop rcx
    add rsp, rcx
    inc rsp
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    push rdi
    cmp rdi, STD_IN
    jge .plus_number
    mov r9, rdi
    mov rdi, SIGN
    push r9
    call print_char
    mov rdi, r9
    neg rdi
    pop r9
.plus_number:
    call print_uint
.end:
    pop rdi
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push r10
    push rdi
    push rsi
    call string_length
    mov r10, rax
    mov rdi, rsi
    call string_length
    mov r11, rax
    pop rsi
    pop rdi
    cmp r10, r11
    jne .not_equal
.equal_len:
    cmp r10, STD_IN
    je .end
    cmpsb
    jne .not_equal
    dec r10
    loop .equal_len
.not_equal:
    pop r10
    xor rax, rax
    ret
.end:
    pop r10
    mov rax, STD_OUT
    ret

; Читает один символ из STD_IN и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push rdi
    dec rsp
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, STD_OUT
    syscall
    test rax, rax
    je .ret_null
    xor rax, rax
    mov al, byte [rsp]
    jmp .end
.ret_null:
    xor rax, rax
.end:
    inc rsp
    pop rdi
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из STDIN, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rdx, rdx
    xor r9, r9
.skip_spaces:
    call read_char
    cmp rax, TAB
    je .skip_spaces
    cmp rax, STR_END
    je .skip_spaces
    cmp rax, SPACE
    je .skip_spaces
.read_cycle:
    cmp r9, rsi
    je .failure
    cmp rax, TAB
    je .successful
    cmp rax, STR_END
    je .successful
    cmp rax, SPACE
    je .successful
    test rax, rax
    je .successful
    mov byte [rdi + r9], al
    inc r9
    call read_char
    jmp .read_cycle
.successful:
    mov rax, rdi
    mov byte [rdi + r9], STD_IN
    jmp .end
    .failure:
    xor rax, rax
    jmp .end
.end:
    mov rdx, r9
    ret
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push rbx
    xor rax, rax
    xor rdx, rdx
    mov r10, STR_END
.try_parse:
    cmp byte [rdi], MAX_NUM
    jge .end
    cmp byte [rdi], MIN_NUM
    jle .end
    push rdx
    mul r10
    pop rdx
    mov bl, byte [rdi]
    add al, bl
    sub al, ASCII
    inc rdi
    inc rdx
    jmp .try_parse
.end:
    pop rbx
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], SIGN
    jne .positive
    mov r9, rdi
    inc r9
    mov rdi, r9
    call parse_uint
    cmp rdx, STD_IN
    je .end
    neg rax
    inc rdx
    jmp .end
.positive:
    call parse_uint
.end:
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
.cp_str:
    cmp rax, rdx
    je .cannot_copy
    mov bl, byte [rdi + rax]
    mov byte [rsi + rax], bl
    cmp byte [rsi + rax], STD_IN
    je .end
    inc rax
    jmp .cp_str
.cannot_copy:
    xor rax, rax
.end:
    ret
